function initWater()
	Water = {
		ovrImg = love.graphics.newImage("img/water.png"),
		levels = { 100, 100 },
		levelDest = { 100, 100 },
		polys = {{},{}},
		phases = {math.random()*math.pi*2,0},
		warningTimers = {0,0},
		warningLevel = 25,
		warningPeriod = 1,
		warningDuty = 0.65
	}

	resetWater()
end

function resetWater()
	Water.levels = {100, 100}
	Water.levelDest = {100, 100}
	Water.warningTimers = {Water.warningPeriod+0.01,Water.warningPeriod+0.01}
end

function math.sign(a)
	if a<0 then return -1 end
	return 1
end

function increaseWater(ndx, amount)
	Water.levelDest[ndx] = math.min(100, Water.levelDest[ndx] + amount)
	playDrink()
end

function decreaseWater(ndx, amount)
	Water.levelDest[ndx] = math.max(0, Water.levelDest[ndx] - amount)
end

function updateWater(dt)
	local levelSpd = 30
	local function updateSingle(ndx)
		-- water level
		Water.phases[ndx] = (Water.phases[ndx] + dt * (4.5 + math.random())) % (math.pi*2)
		if Water.levelDest[ndx] ~= Water.levels[ndx] then
			Water.levels[ndx] = Water.levels[ndx] + math.sign(Water.levelDest[ndx] - Water.levels[ndx]) * dt * levelSpd
			Water.levels[ndx] = math.limit(0, Water.levels[ndx], 100)
		end

		-- water shape
		for xx = 1,36,2 do
			Water.polys[ndx][xx] = xx
			Water.polys[ndx][xx+1] = 102 * (1-Water.levels[ndx]/100) + math.min((math.sin(xx / 12 + Water.phases[ndx]) + 1) * 3, Water.levels[ndx])
		end
		Water.polys[ndx][37] = 36
		Water.polys[ndx][38] = 102
		Water.polys[ndx][39] = 1
		Water.polys[ndx][40] = 102

		-- blink
		if Water.levelDest[ndx] <  Water.warningLevel then
			Water.warningTimers[ndx] = Water.warningTimers[ndx] + dt
			if Water.warningTimers[ndx] > Water.warningPeriod then
				if gamestate == 2 then
					playAlarm()
				end
				Water.warningTimers[ndx] = 0
			end
		end
	end

	updateSingle(1)
	updateSingle(2)
end

function drawWater()
	local sc = getScale()

	local function drawSingleWater(index)
		if Water.levelDest[index] >= Water.warningLevel or 
			Water.warningTimers[index] < Water.warningPeriod * Water.warningDuty then
				local lx = index == 1 and 30 or love.graphics.getWidth()/sc-74
				love.graphics.setColor(63,169,245,128)
				love.graphics.push()
				love.graphics.translate(lx,30)
				love.graphics.polygon("fill",unpack(Water.polys[index]))
				love.graphics.pop()
				love.graphics.setColor(255,255,255)
				love.graphics.draw(Water.ovrImg, lx, 30, 0, 0.3, 0.3)

		end
	end

	drawSingleWater(1)
	drawSingleWater(2)
end
