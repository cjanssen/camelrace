function initMenus()

	gamefont = love.graphics.newFont("fnt/ArabDances.ttf",120)
	love.graphics.setFont(gamefont)


	menuData = {
		bgimg = love.graphics.newImage("img/StartScreen/start_bg2.png"),
		pressspaceimg = love.graphics.newImage("img/StartScreen/space-02.png"),
		logoimg = love.graphics.newImage("img/StartScreen/logo2-01.png"),
		camelimg = love.graphics.newImage("img/StartScreen/camelstanding-01.png"),
		camelshadow = love.graphics.newImage("img/StartScreen/camelshadow.png"),
		winnerimg = love.graphics.newImage("img/winner_txt.png"),
		tiredimg = love.graphics.newImage("img/StartScreen/tiredcamel.png"),
		loserimg = love.graphics.newImage("img/loser_txt.png"),
	}

	creditsData = {
		seqId = 3,
		seqTimer = 0,
		seqPeriod = 3,
		camelAng = 0,
		camelOri = 1,
		camelX = 0,
		camelY = 0,
		ofsX = 0,
		ofsY = 0,
		camelZoom = 1,
		fontX = -worldWidth*worldWidth,
		fontText = "",
	}

	--- shader
	local shadercode = [[
		uniform vec2 refvec = vec2(cos(0.1),sin(0.1));
		uniform vec4 colorOrig = vec4(0.286,0.176,0.058,1);
		uniform vec4 colorDest = vec4(0.741,0.564,0.353,1);
        vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
        {
        	vec2 pos = screen_coords / love_ScreenSize.xy;
        	float amount = dot(refvec, pos) * 1;
            return color*(amount * colorDest + (1-amount)*colorOrig) * Texel(texture, texture_coords);
        }
    ]]
    creditsData.shader = love.graphics.newShader(shadercode)
    creditsData.fontText = "Camel  Race         Berlin  Mini  Game  Jam  July  2014    Birgit  Pohl  Kieran  Turan  Andrej  Svenke  Christiaan  Janssen  Iwan  Gabovitch        Thanks for playing."
end

function resetMenus()
	creditsData.fontX = -worldWidth*worldWidth
end

function updateMenu(dt)
end

function updateCredits(dt)
	local d = creditsData
	d.seqTimer = d.seqTimer + dt
	if d.seqTimer >= d.seqPeriod then
		-- something
		local oldSeqId = d.seqId
		d.seqId = math.random(6)
		if d.seqId == oldSeqId then d.seqId = 7 end
		d.camelOri = math.random(2)*2 - 3
		d.camelZoom = math.random()*2 + 0.1
		d.ofsX = (math.random()-0.5)*500
		if d.ofsX > 50 then d.camelOri = -1 end
		if d.ofsX < -100 then d.camelOri = 1 end
	end
	d.seqTimer = math.max(0, d.seqTimer % d.seqPeriod)

	d.camelX, d.camelY = 0,0
	d.camelAng = 0
	
	if d.seqId == 1 then
		-- vibr	
		local st = d.seqTimer * 8
		local phase = (st % d.seqPeriod) - d.seqPeriod/2
		local halfphase = (st % (d.seqPeriod*2)) - d.seqPeriod
		d.camelAng = (phase * math.sign(halfphase)) * 0.35
		d.camelZoom = 3
		if d.ofsX > -200 then d.camelOri = -1 end
		if d.ofsX < -400 then d.camelOri = 1 end
	elseif d.seqId == 2 then
		-- jump normal
		local phase = (d.seqTimer * 5) % (math.pi*2)
		d.camelY = - math.limit(0, math.sin(phase), 1) * 100 * d.camelZoom
	elseif d.seqId == 3 then
		-- jump flip
		local phase = (d.seqTimer * 3) % (math.pi*2)
		local jmp = math.limit(0, math.sin(phase), 1) > 0 and 1 or 0
		d.camelAng = -d.camelOri* jmp * phase * 2
		d.camelY = - jmp * math.sin(phase) * 200 * d.camelZoom
	elseif d.seqId == 4 then
		-- jump flip
		local phase = (d.seqTimer * 2) % (math.pi*2)
		local jmp = math.limit(0, math.sin(phase), 1) > 0 and 1 or 0
		d.camelAng = d.camelOri * jmp * phase * 4
		d.camelY = - jmp * math.sin(phase) * 400 * d.camelZoom
	elseif d.seqId == 5 then
		-- moonwalk
		d.camelX = d.seqTimer * 150 * -d.camelOri  * d.camelZoom
		d.camelAng = math.abs(math.sin(d.seqTimer * 8 + math.pi/2)*0.2) * d.camelOri
	elseif d.seqId == 6 then
		-- trot
		d.camelX = d.seqTimer * 90 * d.camelOri  * d.camelZoom
		d.camelY = -math.abs(math.sin(d.seqTimer * 8)*50)  * d.camelZoom
		d.camelAng = math.sin(d.seqTimer * 8 + math.pi/2)*0.3
	elseif d.seqId == 7 then
		-- fade
		d.camelZoom = math.max(d.camelZoom * math.pow(0.996,dt *60), 0.2)
		d.camelY = -math.abs(math.sin(d.seqTimer * 8)*50)  * d.camelZoom
		d.camelAng = math.sin(d.seqTimer * 8 + math.pi/2)*0.3
	end


	d.fontX = d.fontX - dt * 150
	local trx = math.max(love.graphics.getWidth() - screenWidth*getScale(), 0)*0.5
	if d.fontX < -gamefont:getWidth(d.fontText)-trx then
		d.fontX = screenWidth + trx
	end
end


function drawMenuScreen()
	love.graphics.push()
	love.graphics.setColor(255,255,255)
	local trx,try = 0,0
	local sc = getScale()
	if love.graphics.getWidth() / screenWidth < love.graphics.getHeight() / screenHeight then
		try = math.max(love.graphics.getHeight() - screenHeight*sc, 0)*0.5
	else
		trx = math.max(love.graphics.getWidth() - screenWidth*sc, 0)*0.5
	end
	love.graphics.draw(menuData.bgimg, 0, 0, 0, love.graphics.getWidth()/menuData.bgimg:getWidth(), love.graphics.getHeight()/menuData.bgimg:getHeight())

	love.graphics.translate(trx,try)
	love.graphics.scale(sc,sc)

	-- show menu
	love.graphics.draw(menuData.camelshadow, screenWidth * 0.5, 200, 0, 1/sc, 1, menuData.camelshadow:getWidth()*0.5,0)
	love.graphics.draw(menuData.camelimg, screenWidth * 0.5, 200, 0, 1, 1, menuData.camelimg:getWidth()*0.5,0)
	love.graphics.draw(menuData.logoimg,screenWidth * 0.5,40, 0, 1, 1, menuData.logoimg:getWidth()*0.5,0)
	love.graphics.draw(menuData.pressspaceimg, screenWidth * 0.5, 600, 0, 0.8, 0.8, menuData.pressspaceimg:getWidth()*0.5,0)
	love.graphics.pop()
end


function drawCreditsScreen()
	local function drawLoser()
		if winner == 2 then
			love.graphics.setColor(255,255,255)
		else
			love.graphics.setColor(200,200,200)
		end
		love.graphics.draw(menuData.camelshadow, screenWidth * 0.165, 500, 0, 0.63, 0.5, 
			menuData.camelshadow:getWidth()*0.5,menuData.camelshadow:getHeight()*0.5)
		love.graphics.draw(menuData.tiredimg, screenWidth * 0.17, 505, 0, 0.5, 0.5, 
			menuData.camelimg:getWidth()*0.5, menuData.camelimg:getHeight()*0.5)
	end

	love.graphics.push()
	love.graphics.setColor(255,255,255)
	local trx,try = 0,0
	local sc = getScale()
	if love.graphics.getWidth() / screenWidth < love.graphics.getHeight() / screenHeight then
		try = math.max(love.graphics.getHeight() - screenHeight*sc, 0)*0.5
	else
		trx = math.max(love.graphics.getWidth() - screenWidth*sc, 0)*0.5
	end

	love.graphics.draw(menuData.bgimg, 0, 0, 0, love.graphics.getWidth()/menuData.bgimg:getWidth(), love.graphics.getHeight()/menuData.bgimg:getHeight())

	love.graphics.translate(trx,try)
	love.graphics.scale(sc,sc)

	drawLoser()
	if winner == 1 then
		love.graphics.setColor(255,255,255)
	else
		love.graphics.setColor(200,200,200)
	end	

	if creditsData.camelOri < 0 then creditsData.camelOri = -1 else creditsData.camelOri = 1 end
	love.graphics.draw(menuData.camelshadow, screenWidth * 0.7 + creditsData.camelX + creditsData.ofsX, 500, 0, 
		creditsData.camelZoom, creditsData.camelZoom, 
		menuData.camelimg:getWidth()*0.5,menuData.camelimg:getHeight()*0.5)
	love.graphics.draw(menuData.camelimg, screenWidth * 0.7 + creditsData.camelX + creditsData.ofsX, 500 + creditsData.camelY, 
		creditsData.camelAng, creditsData.camelOri * creditsData.camelZoom, creditsData.camelZoom, 
		menuData.camelimg:getWidth()*0.5,menuData.camelimg:getHeight()*0.5)

	-- overlay loser again for smaller (distant) winners
	if creditsData.camelZoom <= 0.5 then
		drawLoser()
	end

	love.graphics.setColor(255,255,255)
	-- love.graphics.draw(menuData.logoimg,screenWidth * 0.5,40, 0, 1, 1, menuData.logoimg:getWidth()*0.5,0)
	love.graphics.draw(menuData.pressspaceimg, screenWidth * 0.5, 630, 0, 0.8, 0.8, menuData.pressspaceimg:getWidth()*0.5,0)

	love.graphics.setShader(creditsData.shader)
	love.graphics.print(creditsData.fontText,creditsData.fontX,24)
	love.graphics.setShader()
	love.graphics.pop()
end
