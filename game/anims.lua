function initAnims()

	local strip = love.graphics.newImage("img/camelStrip.png")


	Anims = {
		camelBatch = love.graphics.newSpriteBatch(strip, 1000),
		camelSprites = {},
		frameNdxs = {0,0},
		frameTimers = {math.random(),math.random()},
		frameDelays = {0.1,0.1},
		frameBaseNdx = {1,1},
		frameStripLens = {3,3},
	}

	local w,h = strip:getWidth(), strip:getHeight()
	for i=0,5 do
		table.insert(Anims.camelSprites,
			love.graphics.newQuad(i*400,0,400,266,w,h) )
	end

end

function resetAnims()
end

function updateAnims(dt)
	for i=1,2 do
		-- find out animspeed
		local camel = i==1 and objects.cl1 or objects.cl2
		local vel = getBodyVel(camel.body)
		if Water.levels[i] <= 0 then
			Anims.frameBaseNdx[i] = 6
			Anims.frameDelays[i] = 1
			Anims.frameStripLens[i] = 1
		elseif vel < 20 then
			Anims.frameBaseNdx[i] = 4
			Anims.frameDelays[i] = 0.4
			Anims.frameStripLens[i] = 2
		else
			Anims.frameBaseNdx[i] = 1
			Anims.frameDelays[i] = 60 / (vel+100)
			Anims.frameStripLens[i] = 3
		end

		Anims.frameNdxs[i] = Anims.frameNdxs[i] % Anims.frameStripLens[i]

		-- update the frames
		Anims.frameTimers[i] = Anims.frameTimers[i] + dt
		while Anims.frameTimers[i] > Anims.frameDelays[i] do
			Anims.frameTimers[i] = Anims.frameTimers[i] - Anims.frameDelays[i]
			Anims.frameNdxs[i] = (Anims.frameNdxs[i] + 1) % Anims.frameStripLens[i]
		end

		if Anims.frameStripLens[i] == 1 then Anims.frameNdxs[i] = 0 end
	end
end

function drawCamels()
	Anims.camelBatch:clear()
	drawCamelFrame(objects.cl1, 1)
	drawCamelFrame(objects.cl2, 2)
	love.graphics.setColor(255,255,255)
	love.graphics.draw(Anims.camelBatch)
end

function drawCamelFrame(camel, ndx)
	if ndx == 1 then
		Anims.camelBatch:setColor(255,255,255)
	else
		Anims.camelBatch:setColor(200,200,200)
	end

	local ang = camel.body:getAngle()
	local fndx = Anims.frameBaseNdx[ndx] + Anims.frameNdxs[ndx]
	local x,y = camel.body:getPosition()

	if gamestate == 3 and ndx == winner then
		-- victory dance
		local phase = endTimer * math.pi * 2 * 0.4
		local yinc = math.limit(0,math.sin(phase),1) * 140
		local anginc = math.sin(phase) >= 0 and phase*2 or 0
		y = y - math.cos(ang)*yinc
		x = x + math.sin(ang)*yinc
		ang = ang - anginc
	end


	Anims.camelBatch:add(Anims.camelSprites[fndx], x, y, ang, 0.25, 0.25, 200, 133)

end

