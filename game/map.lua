
function initMap()
	Map = {
		{1,4,3,5,1,1,1,1,},
		{1,2,1,2,1,4,3,5,},
		{1,6,5,2,1,2,1,2,},
		{4,3,8,8,3,7,1,2,},
		{6,3,7,2,1,1,1,2,},
		{1,1,1,6,3,3,3,7,},
	}

	Map.checkPoints = {
		{4,1},{4,3},{4,5},{7,6},{8,4},{7,2},{5,4},{2,4},{2,5},{2,3},
	}

	Player_checkpoints = 1
	checkpointImg = love.graphics.newImage("img/checkpoint.png")
	mapImg = love.graphics.newImage("img/map3.png")
end

function resetMap()
	Player_checkpoints = 1
end


function checkOffroad(camel)
	-- extra modifier: steering

	local dampingFactor = 0.7 + math.abs(camel.body:getAngularVelocity() * 0.3)

	local x,y = camel.body:getX(), camel.body:getY()
	local xx,yy = math.floor(x/256)+1, math.floor(y/256)+1
	local xt,yt = x % 256, y % 256

	if checkNoRoadtile(xx,yy) then
		camel.body:setLinearDamping(10 * dampingFactor)
		return
	end

	local isOffRoad = checkPixelOffroad(xx,yy, xt, yt)

	if isOffRoad then
		camel.body:setLinearDamping(7 * dampingFactor)
	else
		camel.body:setLinearDamping(2 * dampingFactor)
	end

end


function checkCheckpoint(index, camel)
	local x,y = camel.body:getX(), camel.body:getY()
	local xx,yy = math.floor(x/256)+1, math.floor(y/256)+1
	if checkPlayerCP(index, xx, yy) and checkCenterOfTile(x%256,y%256) then
		increaseWater(index, 16)
		increasePlayerCP(index)
	end
end

function checkWaterConsumption(camel, ndx, dt)
	local vel = math.max(getBodyVel(camel.body), 100)
	decreaseWater(ndx, vel*0.01*dt)
	if Water.levels[ndx] <= 0 then
		Water.levelDest[ndx] = 0 -- weird bug
		gamestate = 3
		winner = 3-ndx
		playMusic(4)
	end
end

function checkPlayerCP(playerIndex, xx, yy)

	local desired_pos = Map.checkPoints[Player_checkpoints]
	if xx == desired_pos[1] and yy == desired_pos[2] then
		return true
	end

	return false
end

function increasePlayerCP(playerIndex)
	Player_checkpoints = (Player_checkpoints % #Map.checkPoints) + 1
end


function checkPixelOffroad(xx,yy, xt, yt)
	if not Map[yy] or not Map[yy][xx] then return true end

	local tileIndex = Map[yy][xx]

	if tileIndex == 1 then return true
	elseif tileIndex == 2 then return xt < 64 or xt > 192
	elseif tileIndex == 3 then return yt < 64 or yt > 192
	elseif tileIndex == 4 then return xt < 64 or yt < 64 or (xt > 192 and yt > 192)
	elseif tileIndex == 5 then return xt > 192  or yt < 64 or (xt < 64 and yt > 192)
	elseif tileIndex == 6 then return xt < 64 or yt > 192 or (xt > 192 and yt < 64)
	elseif tileIndex == 7 then return xt > 192 or yt > 192 or (xt < 64 and yt < 64)
	elseif tileIndex == 8 then 
		if xt >= 64 and xt <= 192 then return false end
		if yt >= 64 and yt <= 192 then return false end
		return true
	end


end

function checkNoRoadtile(xx,yy) 
	if not Map[yy] or not Map[yy][xx] then return true end
	return Map[yy][xx] == 1
end

function checkCenterOfTile(xt,yt)
	return xt >= 64 and xt <= 196 and yt >= 64 and yt <= 196
end


function updateMap(dt)

end


function drawMap()
	love.graphics.setColor(255,255,255)
	love.graphics.draw(mapImg)

	local cp = Map.checkPoints[Player_checkpoints]
	love.graphics.draw(checkpointImg, (cp[1]-0.5)*256, (cp[2]-0.5)*256, 0, 0.25, 0.25, 
		checkpointImg:getWidth()*0.5, checkpointImg:getHeight()*0.5)
end