function initDust()
	local img = love.graphics.newImage("img/cloud.png")
	local ps = love.graphics.newParticleSystem(img,300)
	ps:setEmissionRate(40)	
	ps:setSpeed(0, 50)
	ps:setLinearAcceleration(0,40)
	ps:setSizes(0.1, 2.5)
	ps:setColors(190, 133, 77, 255, 100, 65, 17, 0)
	ps:setPosition(400, 300)
	ps:setParticleLifetime(2.5)
	ps:setDirection(-math.pi/2)
	ps:setAreaSpread("uniform", 1,1)
	ps:setOffset(50,50)
	ps:setRadialAcceleration(0,20)
	ps:setRelativeRotation(false)
	ps:setRotation(-math.pi, math.pi)
	ps:setSizeVariation(0)
	ps:setInsertMode("bottom")
	ps:setSpin(-math.pi/2, math.pi/2)
	ps:setSpinVariation(1)
	ps:setSpread(0.8)
	ps:setTangentialAcceleration(-10,10)

	objects.cl1.dust = ps:clone()
	objects.cl1.dust:start()
	objects.cl2.dust = ps:clone()
	objects.cl2.dust:start()


	---- star PS
	local starImg = love.graphics.newImage("img/star.png")
	local starPs = love.graphics.newParticleSystem(starImg, 300)
	starPs:setEmissionRate(0)
	starPs:setSpeed(20, 70)
	starPs:setSizes(0.01, 0.4)
	starPs:setColors(255, 255, 192, 255, 192, 192, 140, 0)
	starPs:setPosition(400, 300)
	starPs:setParticleLifetime(1.5)
	starPs:setAreaSpread("uniform", 5,5)
	starPs:setOffset(50,50)
	starPs:setRadialAcceleration(0,30)
	starPs:setRelativeRotation(false)
	starPs:setRotation(-math.pi, math.pi)
	starPs:setSizeVariation(0)
	starPs:setInsertMode("bottom")
	starPs:setSpin(-math.pi/2, math.pi/2)
	starPs:setSpinVariation(1)
	starPs:setSpread(math.pi*2)
	starPs:setTangentialAcceleration(-40,40)

	objects.cl1.stars = starPs:clone()
	objects.cl1.stars:start()
	objects.cl2.stars = starPs:clone()
	objects.cl2.stars:start()
	generalStars = starPs:clone()
	generalStars:start()
end

function resetDust()
	objects.cl1.dust:reset()
	objects.cl1.stars:reset()
	objects.cl2.dust:reset()
	objects.cl2.stars:reset()
end

function updateDust(dt)
	local function updateSingle(camel)
		local xx,yy = camel.body:getPosition()
		local camelVel = getBodyVel(camel.body)
		local vel = math.max(0,camelVel-200)
		camel.dust:setEmissionRate(vel/3)
		local ang = camel.body:getAngle()
		camel.dust:setDirection(ang+math.pi)
		camel.dust:setLinearAcceleration(math.cos(ang)*40, math.sin(ang)*40)
		local dustang = ang+6.5*math.pi/8
		camel.dust:setPosition(xx + math.cos(dustang) * 50,yy + math.sin(dustang)*50)

		camel.dust:update(dt)

		-- update gallop sound
		camel.gallop.period = 50*math.pow(camelVel,-0.8)
		camel.gallop.timer = camel.gallop.timer + dt
		if camel.gallop.timer >= camel.gallop.period then
			camel.gallop.timer = camel.gallop.timer % camel.gallop.period
			playCamelRun()
		end
	end
	updateSingle(objects.cl1)
	updateSingle(objects.cl2)

	objects.cl1.stars:update(dt)
	objects.cl2.stars:update(dt)
	generalStars:update(dt)
end

function hit(fix1, fix2, contactData)
	local vel1,vel2 = getBodyVel(fix1:getBody()),getBodyVel(fix2:getBody())
	local amount = math.ceil(math.max(vel1,vel2)/20)
	local betweenrocks = true
	if fix1 == objects.cl1.fixture or fix2 == objects.cl1.fixture then
		objects.cl1.stars:setPosition(contactData:getPositions())
		objects.cl1.stars:emit(amount)
		betweenrocks = false
		if fix1 == objects.cl2.fixture or fix2 == objects.cl2.fixture then
			playCamelCamelHit(amount/30)
		else
			playCamelStoneHit(amount/30)
		end
	end
	if fix1 == objects.cl2.fixture or fix2 == objects.cl2.fixture then
		objects.cl2.stars:setPosition(contactData:getPositions())
		objects.cl2.stars:emit(amount)
		betweenrocks = false
		if fix1 == objects.cl1.fixture or fix2 == objects.cl1.fixture then
			playCamelCamelHit(amount/30)
		else
			playCamelStoneHit(amount/30)
		end
	end

	if betweenrocks then
		generalStars:setPosition(contactData:getPositions())
		generalStars:emit(amount)
		playStoneStoneHit()
	end
end


function drawDust()
	love.graphics.setColor(255,255,255)
	love.graphics.draw(generalStars)
	love.graphics.draw(objects.cl1.stars)
	love.graphics.draw(objects.cl2.stars)
	love.graphics.draw(objects.cl1.dust)
	love.graphics.draw(objects.cl2.dust)
end