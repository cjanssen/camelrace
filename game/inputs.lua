-- xbox 1 03000000450c00002043000010010000
-- xbox 2 030000005e0400008e02000020200000
-- pslike 030000008f0e00000300000010010000

local keyBindings = {
	lnitro = "lshift",
	rnitro = "rshift",
	rup = "up",
	rdown = "down",
	rleft = "left",
	rright = "right",
	lup = "w",
	ldown = "s",
	lleft = "a",
	lright = "d"
}

-- note: "rup", "rdown", "rleft" and "rright" as values refer to the right joystick of the controller
local joyBindings = {
	lnitro = {5,6},
	rnitro = {5,6},
	lup = {"up",1,2},
	ldown = "down",
	lleft = "left",
	lright = "right",
	rup = {"up",1,2},
	rdown = "down",
	rleft = "left",
	rright = "right"
}

local invertedJoy = {
	lup = true,
	lleft = true,
	rup = true,
	rleft = true
}

function initInputs()
end

function updateInputs(dt)
end

function drawInputs()
end

function checkKeyBinding(key, id)
	return keyBindings[id] == key
end

function setKeyBinding(key, id)
	keyBindings[id] = key
end

function getKeyBinding(id)
	return keyBindings[id]
end

function checkKeyDown(id)
	return keyBindings[id] and love.keyboard.isDown(keyBindings[id])
end

function checkJoyBinding(button, id)
	local bnd = joyBindings[id]
	if type(bnd) == "table" then
		for i=1,#bnd do
			if bnd[i] == button then return true end
		end
		return false
	end
	-- else
	return joyBindings[id] == button
end

local function deadZone(v)
	if math.abs(v) < 0.15 then return 0 end
	return v
end

local function checkJoyDownSingle(bnd, id, joystick)
	-- buttons
	if type(bnd) == "number" then
		local inv = invertedJoy[id] and -1 or 1
		return joystick:isDown(bnd) and inv or 0
	end

	-- hats
	local hatPressed = joystick:getHat(1)
	if bnd == "up" or bnd == "rup" then
		if hatPressed == "u" or hatPressed == "lu" or hatPressed == "ru" then
			return -1
		end
	end
	if bnd == "down" or bnd == "rdown" then
		if hatPressed == "d" or hatPressed == "ld" or hatPressed == "rd" then
			return 1
		end
	end

	if bnd == "left" or bnd == "rleft" then
		if hatPressed == "l" or hatPressed == "lu" or hatPressed == "ld" then
			return -1
		end
	end

	if bnd == "right" or bnd == "rright" then
		if hatPressed == "r" or hatPressed == "ru" or hatPressed == "rd" then
			return 1
		end
	end

	-- axes
	if bnd == "up" or bnd == "down" then
		return deadZone(joystick:getAxis(2))
	end
	if bnd == "left" or bnd == "right" then
		return deadZone(joystick:getAxis(1))
	end
	if bnd == "rup" or bnd == "rdown" then
		return deadZone(joystick:getAxis(4))
	end
	if bnd == "rleft" or bnd == "rright" then
		return deadZone(joystick:getAxis(3))
	end

	return 0
end

function checkJoyDown(id, joystick)
	if type(joyBindings[id]) == "table" then
		for _,binding in ipairs(joyBindings[id]) do
			local res = checkJoyDownSingle(binding, id, joystick)
			if res ~= 0 then return res end
		end
		return 0
	else 
		return checkJoyDownSingle(joyBindings[id], id, joystick)
	end
end

