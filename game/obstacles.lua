
function loadWorld()
	love.physics.setMeter(64)
	world = love.physics.newWorld(0, 0, true)
	world:setCallbacks(hit)

	objects = {} 

	-- walls
	objects.ground = {}
	objects.ground.body = love.physics.newBody(world, worldWidth/2, worldHeight)
	objects.ground.shape = love.physics.newRectangleShape(worldWidth, 20)
	objects.ground.fixture = love.physics.newFixture(objects.ground.body, objects.ground.shape)

	objects.leftwall = {}
	objects.leftwall.body = love.physics.newBody(world, 0, worldHeight/2)
	objects.leftwall.shape = love.physics.newRectangleShape(20, worldHeight)
	objects.leftwall.fixture = love.physics.newFixture(objects.leftwall.body, objects.leftwall.shape)

	objects.rightwall = {}
	objects.rightwall.body = love.physics.newBody(world, worldWidth, worldHeight/2)
	objects.rightwall.shape = love.physics.newRectangleShape(20, worldHeight)
	objects.rightwall.fixture = love.physics.newFixture(objects.rightwall.body, objects.rightwall.shape)

	objects.topwall = {}
	objects.topwall.body = love.physics.newBody(world, worldWidth/2, 0)
	objects.topwall.shape = love.physics.newRectangleShape(worldWidth, 20)
	objects.topwall.fixture = love.physics.newFixture(objects.topwall.body, objects.topwall.shape)

	-- camels
	objects.cl1 = {}
	objects.cl1.body = love.physics.newBody(world, 550, 96, "dynamic")
	objects.cl1.shape = love.physics.newCircleShape(28)
	objects.cl1.fixture = love.physics.newFixture(objects.cl1.body, objects.cl1.shape)
	objects.cl1.fixture:setRestitution(0.5)
	objects.cl1.body:setAngularDamping(5)
	objects.cl1.body:setLinearDamping(2)

	objects.cl2 = {}
	objects.cl2.body = love.physics.newBody(world, 550, 156, "dynamic")
	objects.cl2.shape = love.physics.newCircleShape(28)
	objects.cl2.fixture = love.physics.newFixture(objects.cl2.body, objects.cl2.shape)
	objects.cl2.fixture:setRestitution(0.5)
	objects.cl2.body:setAngularDamping(5)
	objects.cl2.body:setLinearDamping(2)

	cameras = {
		{
			x = 0,
			y = 0,
			zoom = 1,
		},
		{
			x = 0,
			y = 0,
			zoom = 1,
		}
	}

	polys = {}
	mobilePolys = {}

	createStoredPolys()
end

function createStoredPolys()
	local POLYS = getPolyData()

	local MovablePolys = {80, 81, 82, 83, 84, 85, 86, 87, 88, 89}
	local j = 1

	polycolors = {}
	for i,v in ipairs(POLYS) do
		table.insert(polycolors, {math.random(128)+128, math.random(128)+128, math.random(128)+128, 128})
		local img = nil
		if i==MovablePolys[j] then
			local fname = "img/rock_"..j..".png"
			if love.filesystem.exists(fname) then
				img = love.graphics.newImage(fname)
			end
		end
		createPolyObj(v, img ~= nil, img)
		if i==MovablePolys[j] then j = j + 1 end
	end
end

function createPolyObj(polylist, movable, movableImg)
	local p = {}

	local centerpointx,centerpointy = 0,0
	for i = 1,#polylist/2 do
		centerpointx = centerpointx + polylist[i*2-1]*2/#polylist
		centerpointy = centerpointy + polylist[i*2]*2/#polylist
	end
	for i = 1,#polylist/2 do
		polylist[i*2-1] = polylist[i*2-1] - centerpointx
		polylist[i*2] = polylist[i*2] - centerpointy
	end

	local movableStr = movable and "dynamic" or "static"
	local density = movable and 15 or 2
	p.body = love.physics.newBody(world,centerpointx,centerpointy, movableStr)
	p.shape = love.physics.newPolygonShape(unpack(polylist))
	p.fixture = love.physics.newFixture(p.body, p.shape, density)
	p.img = movableImg
	p.movable = movable
	p.originalPos = { p.body:getPosition() }
	if movable then
		table.insert(mobilePolys, p)
	else
		table.insert(polys, p)
	end
end

function resetObjects()
	for i,p in ipairs(mobilePolys) do
		p.body:setPosition(unpack(p.originalPos))
		p.body:setLinearVelocity(0,0)
		p.body:setAngularVelocity(0,0)
		p.body:setAngle(0)
	end
end


function resetCamels()
	local function resetSingleCamel(camel, x, y)
		camel.body:setPosition(x, y)
		camel.body:setAngle(0)
		camel.body:setLinearVelocity(0,0)
		camel.body:setAngularVelocity(0)
		camel.gallop = {
			timer = 10,
			period = 1,
		}
	end

	resetSingleCamel(objects.cl1, 550, 96)
	resetSingleCamel(objects.cl2, 550, 156)
end

function getBodyVel(body)
	local vx,vy = body:getLinearVelocity()
	return math.sqrt(vx*vx + vy*vy)
end


---------------------------------------------------------

function getPolyData()
	return {
{ 96, 220, 171, 240, 195, 214, 177, 187, 205, 156, 148, 116, 110, 142, },
{ 113, 147, 91, 133, 38, 181, 47, 189, 105, 207, },
{ 240, 117, 268, 139, 293, 133, 319, 90, 312, 79, 279, 73, 238, 93, },
{ 231, 265, 206, 280, 223, 310, 258, 322, 270, 311, },
{ 249, 288, 263, 265, 276, 265, 303, 292, 304, 300, 262, 309, },
{ 466, 321, 486, 269, 574, 258, 629, 272, 599, 315, 522, 327, },
{ 600, 311, 645, 336, 634, 355, 711, 400, 803, 355, 822, 313, },
{ 821, 314, 790, 293, 759, 225, 723, 219, 674, 235, 602, 277, 605, 315, },
{ 581, 265, 614, 226, 647, 221, 675, 252, 622, 281, },
{ 734, 56, 805, 61, 821, 49, 808, 38, 755, 37, 732, 48, },
{ 968, 87, 1013, 65, 1015, 52, 1031, 49, 1066, 70, 1064, 89, 1027, 128, 981, 117, },
{ 997, 294, 1052, 265, 1067, 280, 1056, 308, 1063, 329, 1053, 348, 1020, 344, 1005, 319, },
{ 982, 491, 1033, 479, 1063, 494, 1063, 507, 1043, 526, 1023, 526, 992, 516, },
{ 1002, 789, 1012, 796, 1032, 797, 1049, 765, 1047, 749, 1036, 745, 1014, 768, },
{ 1061, 808, 1118, 813, 1202, 811, 1202, 805, 1156, 792, 1105, 790, 1060, 802, },
{ 1225, 805, 1243, 816, 1279, 821, 1302, 807, 1306, 776, 1281, 759, 1261, 760, 1236, 779, },
{ 1287, 368, 1335, 348, 1362, 322, 1367, 295, 1353, 276, 1296, 287, 1271, 344, },
{ 1273, 342, 1240, 363, 1264, 415, 1249, 777, 1259, 777, 1287, 364, },
{ 1257, 728, 1291, 724, 1279, 711, 1284, 642, 1261, 634, },
{ 1280, 645, 1312, 639, 1296, 553, 1307, 376, 1282, 362, 1266, 648, },
{ 1305, 377, 1321, 383, 1330, 447, 1294, 491, },
{ 1299, 478, 1320, 532, 1315, 560, 1289, 570, },
{ 1258, 661, 1239, 626, 1208, 638, 1244, 492, 1280, 494, },
{ 1245, 497, 1262, 426, 1228, 411, 1232, 363, 1274, 367, 1296, 429, },
{ 1512, 305, 1583, 300, 1593, 291, 1568, 271, 1506, 284, 1502, 296, },
{ 1632, 564, 1666, 554, 1684, 538, 1653, 491, 1640, 497, 1643, 526, 1630, 547, 1630, 563, },
{ 1752, 252, 1750, 280, 1792, 292, 1816, 283, 1807, 265, },
{ 1806, 269, 1825, 218, 1849, 197, 1883, 188, 1908, 203, 1890, 309, 1846, 291, 1812, 283, },
{ 1889, 307, 1910, 315, 1941, 299, 1954, 304, 1978, 289, 1981, 267, 1946, 244, 1905, 203, },
{ 1876, 102, 1857, 131, 1865, 161, 1895, 181, 1914, 161, 1924, 131, 1903, 98, },
{ 1893, 171, 1898, 200, 1907, 208, 1904, 161, },
{ 1702, 612, 1721, 628, 1764, 633, 1811, 625, 1826, 611, 1800, 572, 1717, 570, },
{ 1719, 575, 1750, 510, 1797, 505, 1811, 491, 1833, 495, 1848, 545, 1827, 576, 1766, 586, },
{ 1532, 1299, 1665, 1319, 1753, 1313, 1790, 1297, 1827, 1052, 1797, 1033, 1517, 1229, },
{ 1531, 1278, 1450, 1194, 1439, 1156, 1482, 1074, 1513, 1056, 1699, 1120, },
{ 1534, 1069, 1598, 839, 1680, 763, 1727, 766, 1827, 884, 1827, 918, 1796, 1042, 1675, 1137, },
{ 1499, 1064, 1499, 944, 1510, 959, 1515, 1064, },
{ 1473, 1016, 1517, 1036, 1527, 1001, 1499, 976, 1478, 979, },
{ 1497, 944, 1480, 967, 1493, 989, 1506, 974, 1516, 950, },
{ 1596, 738, 1631, 738, 1618, 826, 1608, 832, },
{ 1583, 756, 1567, 783, 1587, 805, 1624, 787, 1645, 755, },
{ 1692, 648, 1659, 676, 1666, 716, 1694, 737, 1721, 729, 1739, 691, 1715, 649, },
{ 1698, 728, 1703, 779, 1718, 781, 1709, 722, },
{ 1255, 987, 1224, 1004, 1234, 1013, 1330, 1007, 1339, 989, 1323, 985, },
{ 1335, 989, 1378, 989, 1395, 999, 1386, 1013, 1351, 1016, 1328, 1005, },
{ 1376, 991, 1383, 973, 1427, 972, 1437, 977, 1436, 989, 1392, 1000, },
{ 1074, 1036, 1106, 1056, 1101, 1061, 1049, 1061, 1025, 1070, 994, 1073, 987, 1063, 997, 1038, },
{ 998, 1042, 982, 1020, 985, 1013, 1056, 992, 1083, 1016, 1084, 1032, 1065, 1044, },
{ 1119, 1297, 1126, 1309, 1224, 1313, 1355, 1299, 1444, 1286, 1411, 1270, 1335, 1266, 1244, 1277, },
{ 1935, 1500, 1983, 1505, 2009, 1495, 2009, 1456, 1999, 1423, 1970, 1427, 1933, 1480, },
{ 1203, 1479, 1260, 1481, 1268, 1491, 1262, 1500, 1221, 1504, 1202, 1494, },
{ 1114, 1470, 1101, 1480, 1098, 1505, 1118, 1507, 1129, 1498, 1125, 1478, },
{ 1104, 1488, 1035, 1473, 1015, 1480, 998, 1497, 1023, 1510, 1102, 1504, },
{ 711, 1510, 733, 1512, 740, 1354, 711, 1350, },
{ 730, 1417, 756, 1385, 807, 1356, 809, 1328, 743, 1285, 663, 1325, 723, 1418, },
{ 785, 1320, 819, 1295, 807, 1249, 749, 1184, 689, 1190, 668, 1225, 693, 1320, },
{ 678, 1225, 628, 1210, 574, 1236, 550, 1285, 623, 1312, 712, 1320, },
{ 554, 1273, 524, 1294, 527, 1308, 553, 1313, 571, 1300, },
{ 334, 1379, 335, 1343, 279, 1298, 261, 1293, 242, 1313, 284, 1380, },
{ 268, 1350, 231, 1224, 160, 1203, 143, 1206, 77, 1369, 189, 1378, },
{ 140, 1215, 74, 1256, 38, 1302, 40, 1328, 82, 1369, },
{ 405, 1441, 533, 1355, 599, 1370, 593, 1435, 548, 1506, 398, 1512, },
{ 407, 1445, 317, 1403, 251, 1413, 254, 1482, 325, 1488, 376, 1516, 438, 1483, },
{ 720, 1054, 759, 1063, 800, 1059, 824, 1038, 828, 1004, 745, 967, 691, 999, 693, 1023, },
{ 513, 1045, 527, 1052, 544, 1030, 536, 996, 511, 1002, 507, 1028, },
{ 490, 724, 441, 736, 433, 761, 465, 799, 520, 739, },
{ 467, 796, 472, 812, 522, 824, 565, 814, 576, 776, 562, 751, 516, 739, },
{ 240, 816, 221, 821, 103, 752, 82, 660, 251, 592, 303, 665, },
{ 91, 660, 79, 609, 104, 540, 145, 516, 220, 540, 253, 600, },
{ 104, 545, 80, 540, 86, 503, 139, 480, 182, 518, },
{ 465, 540, 489, 553, 535, 538, 548, 514, 514, 501, 475, 516, },
{ 534, 534, 543, 550, 564, 559, 588, 546, 591, 525, 572, 503, 531, 513, },
{ 511, 504, 528, 461, 570, 471, 556, 528, },
{ 529, 475, 508, 462, 482, 466, 487, 415, 580, 384, 689, 432, 604, 503, },
{ 733, 407, 826, 448, 823, 487, 709, 569, 602, 492, 679, 435, },
{ 786, 508, 828, 540, 822, 578, 732, 625, 694, 549, },
{ 761, 577, 830, 627, 823, 664, 723, 676, 706, 613, },
{ 725, 671, 747, 691, 777, 705, 798, 703, 787, 652, },
{ 777, 701, 739, 739, 746, 780, 807, 775, 821, 763, 794, 696, },
-- following are movable
{ 1025, 1265, 1008, 1285, 1007, 1294, 1022, 1309, 1055, 1316, 1069, 1296, 1065, 1277, },
{ 505, 1264, 494, 1269, 493, 1294, 511, 1296, 524, 1289, 520, 1271, },
{ 722, 1132, 737, 1140, 754, 1138, 765, 1126, 748, 1117, 737, 1119, },
{ 1500, 522, 1507, 529, 1533, 521, 1546, 495, 1542, 488, 1519, 495, },
{ 990, 586, 968, 601, 979, 616, 991, 613, 999, 594, },
{ 404, 1055, 418, 1057, 432, 1049, 433, 1042, 424, 1035, 406, 1046, },
{ 284, 1062, 262, 1050, 261, 1043, 276, 1030, 292, 1037, 293, 1056, },
{ 278, 1015, 296, 1024, 316, 1022, 318, 1014, 303, 1003, 286, 1004, },
{ 253, 1022, 272, 1012, 273, 1005, 266, 995, 240, 999, 231, 1011, 239, 1021, },
{ 343, 752, 352, 761, 368, 758, 388, 746, 381, 735, 370, 734, },
}
end
