function loadSounds()
	Sounds = { active = true }
	Sounds.bell = {
		love.audio.newSource("snd/bell1.ogg","static"),
		love.audio.newSource("snd/bell2.ogg","static")
	}

	Sounds.alarm = love.audio.newSource("snd/alarm5.ogg","static")
	Sounds.gallop = love.audio.newSource("snd/gallop2.ogg","static")
	Sounds.shout = love.audio.newSource("snd/camelshout.ogg","static")
	Sounds.camelCamel = love.audio.newSource("snd/ouch1.ogg","static")
	Sounds.camelStone = love.audio.newSource("snd/punchi1.ogg","static")
	Sounds.stoneStone = love.audio.newSource("snd/stone2.ogg","static")
	Sounds.water =  love.audio.newSource("snd/water1.ogg","static")


	-- music
	local _musicName = {
		"snd/intro.ogg",
		"snd/start.ogg",
		"snd/main.ogg",
		"snd/outro.ogg",
		"snd/end.ogg",
	}
	Sounds.music = {}
	for i=1,#_musicName do
		Sounds.music[i] = love.audio.newSource(_musicName[i], "stream")
	end
	Sounds.music[1]:setLooping(true)
	Sounds.music[3]:setLooping(true)
	Sounds.music[5]:setLooping(true)
	Sounds.musicTimer = 10
end

-- function love.livereload()
-- 	loadSounds()
-- end

function playStoneStoneHit()
	if Sounds.active then
		local snd = Sounds.stoneStone:clone()
		snd:setVolume(0.5)
		snd:play()
	end
end

function playCamelStoneHit(vol)
	if Sounds.active then
		local snd = Sounds.camelStone:clone()
		snd:setVolume(math.min(vol,1))
		snd:play()
	end
end

function playCamelCamelHit(vol)
	if Sounds.active then
		local snd = Sounds.camelCamel:clone()
		snd:setVolume(math.min(vol,1))
		snd:play()
	end
end

function playCamelRun()
	if Sounds.active then
		local snd = Sounds.gallop:clone()
		snd:setVolume(0.6)
		snd:play()
	end
end

function playCamelAccel()
	if Sounds.active then
		local snd = Sounds.shout:clone()
		snd:setVolume(0.5)
		snd:play()
	end
end

function playDrink()
	if Sounds.active then
		local snd = Sounds.water:clone()
		snd:setVolume(0.5)
		snd:play()
	end
end

function playSemaphore(n)
	if Sounds.active then
		local snd = Sounds.bell[n]
		snd:setVolume(0.5)
		snd:play()
	end
end

function playAlarm()
	if Sounds.active then
		local snd = Sounds.alarm:clone()
		snd:setVolume(0.5)
		snd:play()
	end
end

function updateMusic(dt)
	if Sounds.active then
		local mlen = 8.87140
		if Sounds.musicTimer < mlen then
			Sounds.musicTimer = Sounds.musicTimer + dt
			if Sounds.musicTimer >= mlen then
				playMusic(3)
			end
		end
	end
end

function playMusic(part)
	if Sounds.active then
		if part == 2 then
			Sounds.musicTimer = 0
		end
		for i=1,#Sounds.music do
			Sounds.music[i]:stop()
		end
		Sounds.music[part]:play()
	end
end

