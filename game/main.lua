function love.load()
	love.filesystem.load("map.lua")()
	love.filesystem.load("water.lua")()
	love.filesystem.load("obstacles.lua")()
	love.filesystem.load("anims.lua")()
	love.filesystem.load("dust.lua")()
	love.filesystem.load("menus.lua")()
	love.filesystem.load("sounds.lua")()
	love.filesystem.load("inputs.lua")()

	worldWidth,worldHeight = 2048, 1536
	screenWidth,screenHeight = 1024, 768
	-- local fullscreen = false
	-- love.window.setMode(800, 600, {fullscreen=fullscreen, fullscreentype="desktop", resizable=true})
	initInputs()
	initData()

	if love.livereload then
		love.livereload()
	end

	resetGame()
end

function initData()
	loadWorld()

	initMap()
	initSemaphore()
	initWater()
	initAnims()
	initDust()
	initMenus()
	loadSounds()
end

function resetGame()
	gamestate = 1
	resetMenus()
	playMusic(1)
end

function newGame()
	gamestate = 2
	winner = 0
	endTimer = 0
	resetWater()
	resetCamels()
	resetDust()
	resetAnims()
	resetObjects()
	resetMap()
	resetSemaphore()
	playMusic(2)
end

function initSemaphore()
	Semaphore = {
		timer = 0,
		period = 1.5,
		img = love.graphics.newImage("img/semaphore.png"),
		quads = {},
		state = 1,
		opacity = 1,
	}

	for i=0,2 do
		Semaphore.quads[i+1] = love.graphics.newQuad(i*95,0,95,250,285,250)
	end
end

function resetSemaphore()
	Semaphore.timer = Semaphore.period*3
	Semaphore.state = 1
	Semaphore.oldState = 4
end

function math.limit(min, val, max)
	return math.max(min, math.min(val, max))
end

function updateCamera(dt)
	cameras[1].x = math.limit(screenWidth/4, objects.cl1.body:getX(), worldWidth - screenWidth/4)
	cameras[1].y = math.limit(screenHeight/4, objects.cl1.body:getY(), worldHeight - screenHeight/4)
	cameras[2].x = math.limit(screenWidth/4, objects.cl2.body:getX(), worldWidth - screenWidth/4)
	cameras[2].y = math.limit(screenHeight/4, objects.cl2.body:getY(), worldHeight - screenHeight/4)
end

function updateSemaphore(dt)
	if Semaphore.timer > 0 then
		Semaphore.timer = Semaphore.timer - dt
		Semaphore.state = 3 - math.floor(Semaphore.timer/Semaphore.period)
		if Semaphore.state ~= Semaphore.oldState then
			if Semaphore.state < 4 then
				playSemaphore(Semaphore.state == 3 and 2 or 1)
			end
			Semaphore.oldState = Semaphore.state
		end
		Semaphore.opacity = math.limit(0, Semaphore.timer / Semaphore.period, 1)
	end
end

function doNitro(camel, ndx)
	local nitroforce = 300
	local angle = camel.body:getAngle()
 	camel.body:applyLinearImpulse(math.cos(angle)*nitroforce, math.sin(angle)*nitroforce)
	decreaseWater(ndx, 7)
	playCamelAccel()
end

function love.keypressed(key)
 	if key == "escape" then
 		if gamestate ~= 1 then 
 			resetGame()
 		else
	 		love.event.push("quit")
	 	end
 		return
 	end

 	if key == "f1" then
 		local fs,fst = love.window.getFullscreen()
 		love.window.setFullscreen(not fs, fst)
 	end

 	if gamestate == 1 and key == " " then
 		newGame()
 		return
 	end

 	if gamestate == 4 and key == " " then
 		resetGame()
 		return
 	end

 	if gamestate == 2 and Semaphore.timer <= Semaphore.period then
 		-- "nitro"
 		if checkKeyBinding(key, "lnitro") then
 			doNitro(objects.cl1, 1)
 		end
 		if checkKeyBinding(key,"rnitro") then
 			doNitro(objects.cl2, 2)
 		end
 	end

end

function love.joystickpressed(joy, botan)
 	if gamestate == 2  and Semaphore.timer <= Semaphore.period then
 		-- "nitro"
 		
 		if joy == joyz[1] and checkJoyBinding(botan, "lnitro") then
 			doNitro(objects.cl1, 1)
 		end
 		if joy == joyz[2] and checkJoyBinding(botan, "rnitro") then
 			doNitro(objects.cl2, 2)
 		end
 	end

 	if gamestate == 1 then
		newGame()
		return
	end

	if gamestate == 4 then
		resetGame()
		return
	end
end


function updateIngame(dt)
	-- move camels
	checkOffroad(objects.cl1)
	checkOffroad(objects.cl2)

	checkCheckpoint(1, objects.cl1)
	checkCheckpoint(2, objects.cl2)

	-- update physics simulation
	world:update(dt) 

	-- water consumption
	checkWaterConsumption(objects.cl1, 1, dt)
	checkWaterConsumption(objects.cl2, 2, dt)

	-- particle effects
	updateDust(dt)

	-- read input
	if Semaphore.timer <= Semaphore.period then
		updateKeyboard()
		updateJoysticks()
	end

	-- update other subsystems
	updateCamera(dt)
	updateWater(dt)
	updateAnims(dt)
	updateSemaphore(dt)
	updateMusic(dt)

end

function updateEndgame(dt)
	-- move camels
	checkOffroad(objects.cl1)
	checkOffroad(objects.cl2)

	-- update physics simulation
	world:update(dt) 

	-- particle effects
	updateDust(dt)

	-- update other subsystems
	updateCamera(dt)
	updateWater(dt)
	updateAnims(dt)

	-- end animation
	endTimer = endTimer + dt
end

function love.update(dt)
	if gamestate == 1 then
		updateMenu(dt)
	elseif gamestate == 2 then
		updateIngame(dt)
	elseif gamestate == 3 then
		updateEndgame(dt)
	elseif gamestate == 4 then
		updateCredits(dt)
	end
end

function updateKeyboard()
 	local accelforce = 400
 	local stopforce = 120
 	local rotforce = 2000

	-- cl1 controls
  	if checkKeyDown("lup") then
 		-- accelerate
 		local angle = objects.cl1.body:getAngle()
 		objects.cl1.body:applyForce(math.cos(angle)*accelforce, math.sin(angle)*accelforce)
 	end
 	if checkKeyDown("ldown") then
 		-- decelerate
 		local angle = objects.cl1.body:getAngle() + math.pi
 		objects.cl1.body:applyForce(math.cos(angle)*stopforce, math.sin(angle)*stopforce)
 	end
 	if checkKeyDown("lleft") then
 		-- turn left
 		objects.cl1.body:applyTorque(-rotforce)
	end
	if checkKeyDown("lright") then
		-- turn right
 		objects.cl1.body:applyTorque(rotforce)
	end

	-- cl2 controls
  	if checkKeyDown("rup") then
 		-- accelerate
 		local angle = objects.cl2.body:getAngle()
 		objects.cl2.body:applyForce(math.cos(angle)*accelforce, math.sin(angle)*accelforce)
 	end
 	if checkKeyDown("rdown") then
 		-- decelerate
 		local angle = objects.cl2.body:getAngle() + math.pi
 		objects.cl2.body:applyForce(math.cos(angle)*stopforce, math.sin(angle)*stopforce)
 	end
 	if checkKeyDown("rleft") then
 		-- turn left
 		objects.cl2.body:applyTorque(-rotforce)
	end
	if checkKeyDown("rright") then
		-- turn right
 		objects.cl2.body:applyTorque(rotforce)
	end
end

function updateJoysticks()
	joyz = joyz or {}
	local joysticks = love.joystick.getJoysticks()
	local jc = 1
	for i=1,#joysticks do
		local joy = joysticks[i]
		if joy:getButtonCount() >= 3 then
			joyz[jc] = joy
			local affectedBody = jc == 1 and objects.cl1.body or objects.cl2.body

			local accelforce = 400
			local stopforce = 120
			local rotforce = 2000

			local function owndir(dir) return (jc == 1 and "r" or "l")..dir end

			local u,d,l,r = checkJoyDown(owndir("up"), joy), checkJoyDown(owndir("down"), joy), 
						checkJoyDown(owndir("left"), joy), checkJoyDown(owndir("right"), joy)

			if u < 0 then
				local angle = affectedBody:getAngle()
 				affectedBody:applyForce(math.cos(angle)*accelforce*math.abs(u), math.sin(angle)*accelforce*math.abs(u))
 			end
 			if d > 0 then
 				local angle = affectedBody:getAngle() + math.pi
 				affectedBody:applyForce(math.cos(angle)*stopforce*d, math.sin(angle)*stopforce*d)
 			end
 			if l < 0 then
 				affectedBody:applyTorque(rotforce*l)
 			end
 			if r > 0 then
 				affectedBody:applyTorque(rotforce*r)
 			end
			jc = jc + 1
		end
		if jc > 2 then return end
	end
end

function drawEndAnim(camel, ndx)
	if endTimer > 0 then
		local x1,y1 = camel.body:getX() - cameras[ndx].x, camel.body:getY() - cameras[ndx].y
		local ang = camel.body:getAngle()
		local x = x1 * math.cos(ang) + y1 * math.sin(ang)
		local y = -x1 * math.sin(ang) + y1 * math.cos(ang)
		local r = screenHeight * math.limit(0.2, (8 - endTimer) / 8, 1)
		if endTimer > 8 then
			r = screenHeight * math.limit(0, (9-endTimer), 0.2)
		end

		local function hole()
			love.graphics.circle("fill", x, y , r)
		end

		-- compensate camel 
		love.graphics.push()
		love.graphics.translate(cameras[ndx].x, cameras[ndx].y)
		love.graphics.rotate(ang)

		love.graphics.setInvertedStencil(hole)
		love.graphics.setColor(0,0,0,168)
		love.graphics.rectangle("fill",-screenWidth,-screenHeight, screenWidth*2, screenHeight*2)
		love.graphics.setInvertedStencil()

		love.graphics.setColor(255,255,255, math.min(endTimer*70,255))
		if ndx == winner then
			love.graphics.draw(menuData.winnerimg, 0, 200, 0, 1, 1, menuData.winnerimg:getWidth()*0.5, 0)
		else
			love.graphics.draw(menuData.loserimg, 0, 200, 0, 1, 1, menuData.loserimg:getWidth()*0.5, 0)
		end

		love.graphics.pop()

		if r <= 0 then
			gamestate = 4
			playMusic(5)
		end
	end
end

function drawCamelView()
	drawMap()

	love.graphics.setColor(255,255,255)
	for i,v in ipairs(mobilePolys) do
	  	if v.img then
	  		local x,y = v.body:getPosition()
		  	love.graphics.draw(v.img, x,y, v.body:getAngle(), 1, 1, v.img:getWidth()*0.5, v.img:getHeight()*0.5)
		end
	end

	drawCamels()
	drawDust()
end

function drawSemaphores()
	if Semaphore.timer > 0 then
		local sc = getScale()
		love.graphics.setColor(255,255,255,Semaphore.opacity * 255)
		love.graphics.draw(Semaphore.img, Semaphore.quads[Semaphore.state], love.graphics.getWidth()/2/sc, 30, 0, 0.5, 0.5, 95*0.5, 0)
	end
end

function getScale()
	return math.min(love.graphics.getWidth()/screenWidth, love.graphics.getHeight()/screenHeight)
end


function drawGame()
	love.graphics.push()
	local sc = getScale()
	love.graphics.scale(sc,sc)


	-- camel 1 view
	local sw,sh = love.graphics.getWidth(), love.graphics.getHeight()
	love.graphics.push()
	love.graphics.setScissor(0,0,sw/2,sh)
	love.graphics.translate(sw/4/sc, sh/2/sc)
	love.graphics.rotate(-objects.cl1.body:getAngle())
	love.graphics.translate(-cameras[1].x, -cameras[1].y)

	drawCamelView()

	drawEndAnim(objects.cl1, 1)
	love.graphics.pop()

	-- camel 2 view
	love.graphics.push()
	love.graphics.setScissor(sw/2,0,sw/2,sh)
	love.graphics.translate(3*sw/4/sc, sh/2/sc)
	love.graphics.rotate(-objects.cl2.body:getAngle())
	love.graphics.translate(-cameras[2].x, -cameras[2].y)

	drawCamelView()

	drawEndAnim(objects.cl2, 2)
	love.graphics.pop()
	love.graphics.setScissor()

	-- water display
	drawWater()
	drawSemaphores()
	love.graphics.pop()
end


function love.draw()
	love.graphics.setBackgroundColor(173,152,107)
	if gamestate == 1 then
		drawMenuScreen()
	elseif gamestate == 2 or gamestate == 3 then
		drawGame()
	elseif gamestate == 4 then
		drawCreditsScreen()
	end
end


