CamelRace
=========

A 2-player split-screen racing game.
Made at the Berlin Mini Game Jam July 2014

Team
----
    Birgit Pohl
    Kieran Turan
    Andrej Svenke
    Christiaan Janssen
    Iwan Gabovitch
   
Engine
------
Löve2D 0.9.1

If the game doesn't work straight away, make sure that you have Löve2D 0.9.1 installed in your system.  http://love2d.org

Description
-----------
The goal is to collect water in order to keep running.  First player who runs out of water loses.  Sprinting consumes water.  You can play with keyboard or up to 2 gamepads (automatically detected)

Controls:
---------

    F1 - Toggle fullscreen (window is resizable)
    ESC - Exit

Left Player:

    WASD - movement
    left shift - sprint

or

    gamepad-1 joystick - turn
    gamepad-1 1-4 buttons - run
    gamepad-1 shoulder buttons - sprint

Right Player:

    cursor keys - movement
    right shift - sprint
    
or

    gamepad-2 joystick - turn
    gamepad-2 1-4 buttons - run
    gamepad-2 shoulder buttons - sprint
    

